const os = require('os');

const core = os.cpus().length;

module.exports.cpuCores = core;